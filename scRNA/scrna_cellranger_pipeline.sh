#!/bin/bash -l 
#SBATCH --nodes=1 
#SBATCH --ntasks=1
#SBATCH --job-name=scRNA-Pipeline
#SBATCH --time=24:00:00  
#SBATCH --mem=2G
#SBATCH --mail-user=hssgenomics@gmail.com 
#SBATCH --mail-type=END,FAIL

set +x

source ~/.bash_profile

# this scipt is a test template for spinning up jobs and waiting for them
# to complete before moving on. 
# In other words, if a script needs to do something to all samples in a set
# we can we throw these jobs to the server queue and wait for them to complete
# before moving on. 
# This would solve our pipelining issue of having a Perl script that writes
# a bash script for each sample. And then a separate summarize script for
# when all those piplines complete.

err() {
  echo "$1...exiting"
  exit 1
}
ckRes() {
  if [ "$1" == "0" ]; then
    echo "..Done $2 `date`"
  else
    err "$2 returned non-0 exit code $1"
  fi
}

# General usage statement
usage() { 
  echo "Usage: qsub this script (scrna_cellranger_pipeline.sh) in the directory where you want the results output.
  -d destination -- output directory path. Ideally an Analysis directory
  -n name        -- sample name that matches all samples (e.g. Sample_KL).
  -g genome      -- hg38 or mm10 (only mm10 and hg38 are supported by cell_ranger).
  -l libraries   -- csv input that contains fastq location, sample name and library type.
  -b bams        -- boolean <true|false> controlling whether to save bams or not. Default: true
  -s skip        -- skip cellranger aggr step? Default: TRUE
  -i introns     -- boolean <true|false> perform intron counting for snRNA-seq. Default: false
  -h help        -- prints this usage message." 
  exit 1
}

#
# Variables
#
DESTDIR=""    # Destination directory (should be corresponding analysis dir)
NAME=""       # The name common to all samples in the experiment
GENOME=""     # The genome build that that the samples were aligned to
LIBRARIES=""  # The csv file that contains directory of fastqs
BAMS=true	      # The boolean option that choose to save bam files or not
SKIP=TRUE
INTRON=false

#
# Parse command-line arguments
#
while getopts :f:d:n:g:l:b:s:h: opt; do
  case $opt in
    d)
      DESTDIR=$OPTARG
      ;;
    n)
      NAME=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    l)
      LIBRARIES=$OPTARG
      ;;
    b)
      BAMS=$OPTARG
      ;;
    s)
      SKIP=$OPTARG
      ;;
    i)
      INTRON=$OPTARG
      ;;
    h)
      usage
      ;;
    *) 
      usage
      ;;
  esac
done


#
# Check validity of inputs
#
if [[ ! $GENOME =~ hg38|mm10 ]]; then
  echo "Invalid genome selected (only mm10 and hg38 are valid for cell_ranger). \n"
  usage
elif [ "$NAME" == "" ]; then
  echo "Invalid Sample ID. \n"
  usage
elif [ "$LIBRARIES" == "" ]; then
  echo "Invalid library file. \n"
  usage
elif [[ $BAMS =~ true|false ]]; then
  echo "Invalid saving bam option. \n"
  usage
elif [[ ! $SKIP =~ T|true|True|TRUE|F|false|False|FALSE ]]; then
  echo "Invalid skip option passed to -s. \n"
  usage
elif [[ ! $INTRON =~ true|false  ]]; then
  echo "Invalid intron parameter -i. \n"
  usage
fi

# set variables and load packages
GENOMEPATH=/athena/hssgenomics/scratch/genomes_2/$GENOME/Genome/CellRanger/refdata-gex*
SLURMHEADER=./slurm.header

# set variables for this qsub loop (this shouldn't need to be changed by user
# so we don't make these global options
THREADS=16
MEMFREE=64


echo "sample_id,molecule_h5" > aggr.csv

SAMPLEDIRS=(`tail -n +2 $LIBRARIES | cut -d',' -f1`)

echo $SAMPLEDIRS

for (( i=0; i<${#SAMPLEDIRS[*]}; i++ )); do
  SLURMNAME=`basename ${SAMPLEDIRS[$i]}`
  ID=${SLURMNAME}_CellRanger
  FASTQNAME=`ls ${SAMPLEDIRS[$i]} | grep -P -o '.+?(?=_S[0-9]{1,2}_L[0-9]{3})' | uniq`
  ### Build Job Commands
  TOCPCMD="cp -r $GENOMEPATH \$TMPDIR"

  SLURMCMD="cd \$TMPDIR; time cellranger count --disable-ui --jobmode=local --localmem=$MEMFREE --localcores=$THREADS --id=$ID --transcriptome=\$TMPDIR/`basename $GENOMEPATH` --create-bam=$BAMS --fastqs=${SAMPLEDIRS[$i]} --sample=$FASTQNAME --include-introns=$INTRON"
   MVCMD="mkdir -p $DESTDIR; mv \$TMPDIR/$ID $DESTDIR"
  # build the each qsub script and submit
  sed '
s@__JOB_NAME__@'"$SLURMNAME"'@
s@__THREADS__@'"$THREADS"'@
s@__MEMFREE__@'"$MEMFREE"'@
s@__TOCPCMD__@'"$TOCPCMD"'@
s@__SLURMCMD__@'"$SLURMCMD"'@
s@__MVCMD__@'"$MVCMD"'@  
' $SLURMHEADER > ${SLURMNAME}_slurm_script.sh 
  # chmod a+x ${SLURMNAME}_slurm_script.sh
  # qsub the script
  if [ "$JOBIDS" == "" ]; then
    JOBIDS=`sbatch ${SLURMNAME}_slurm_script.sh | cut -d' ' -f4` # Submitted batch job ->JOBID<-
  else
    JOBIDS=$JOBIDS:`sbatch ${SLURMNAME}_slurm_script.sh | cut -d' ' -f4`
  fi
  echo "$ID,$DESTDIR/$ID/outs/molecule_info.h5" >> aggr.csv
done

if [[ $SKIP =~ F|false|False|FALSE ]];
  AGGRCMD="cellranger aggr --disable-ui --jobmode=local --localmem=$MEMFREE --localcores=$THREADS --id=$NAME --csv=aggr.csv --normalize=none"
  SLURMNAME=${NAME}_Aggregate
  sed '
s@__JOB_NAME__@'"$SLURMNAME"'@
s@__THREADS__@'"$THREADS"'@
s@__MEMFREE__@'"$MEMFREE"'@
s@__TOCPCMD__@@
s@__SLURMCMD__@'"$AGGRCMD"'@
s@__MVCMD__@@
' $SLURMHEADER > ${SLURMNAME}_slurm_script.sh 

  sbatch --dependency=afterok:${JOBIDS} ${SLURMNAME}_slurm_script.sh
fi

exit 0
