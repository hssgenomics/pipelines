#!/bin/bash -l 
#SBATCH --partition=panda 
#SBATCH --cpus-per-task=1 
#SBATCH --ntasks=1
<<<<<<< HEAD
#SBATCH --job-name=CHIP-ATAC
#SBATCH --time=24:00:00  
#SBATCH --mem=2G
=======
#SBATCH --job-name=bulkRNA-Pipeline
#SBATCH --time=72:00:00  
#SBATCH --mem=4G
>>>>>>> xchip_revert
#SBATCH --mail-user=hssgenomics@gmail.com 
#SBATCH --mail-type=END,FAIL,TIMEOUT

# set -x

# ///////////////////////////////////////////////////////////////////////////
#
# qsub scripts are executed in the CURRENT WORKING DIRECTORY
# this means that all scripts in the CWD directory can
# be called using ./myscript.sh
#
# This pipeline follows a heirarchical rationale that should allow for modular
# development/improvements over time with minimal effort.
#
# xchip_scheduler.sh : builds sbash scripts from scripts and templates
# |
# |-- slurmExperiment.template : performs all sample related processing
# |   |
# |   |-- copy to tmpdir
# |   |-- read filtering
# |   |-- qc
# |   |-- align
# |   |-- alignment filtering
# |   |-- copy back to cwd 
# |
# |-- slurmGroup.template : performs all group level processing
# |   |
# |   |-- call peaks
# |   |-- make cutsite matrix
# |   |-- footprinting 
# |  
# |-- slurmExperiment.template : perform all experiment level processing
#     | 
#     |-- aggregate stats (multiqc)
#     |-- 
#
# The scripts called by each of the above slurm scripts are located
# in similarly structured `scripts` directory (template.sh is useful)
# 
# scripts
# ├── experiment
# │   └── runmultiqc.sh
# ├── group
# │   └── callpeaks.sh
# ├── sample
# │   ├── cpgenome.sh
# │   ├── runalign.sh
# │   ├── samplecleanup.sh
# └── template.sh
#
#
# NOTES for further development:
# 
# Replacements within template are formed as below (with the exception of slurm header)
# 1. they start and end with 2 underscore
# 2. they start with lowercase do followed by camel case descriptive
# ex. __doSomeThing__ / __doRunAlign__
#
# Function calls are stored as variables following the convention below
# 1. lowecase first descriptive word (usually abbreviated)
# 2. all uppercase second descriptive word
# e.x. runALIGN / callPEAKS
#
# non-function call variables are ALWAYS uppercase
# e.x. MODE / GROUP / ID
#
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

<<<<<<< HEAD
#~~~~~~~~~~~~~~~~~
# helper functions
#~~~~~~~~~~~~~~~~~

=======
>>>>>>> xchip_revert
# monitor: given an array of job IDs, monitor their progress
function monitor {
  # get the parameters from command line
  local delay="$1"
  shift
  local jobarr=("$@")
  # set status and start looping
  local stat="INCOMPLETE"
  while [ "$stat" = "INCOMPLETE" ]; do
    if [ ${#jobarr[@]} -gt 0 ]; then
      local checkstat=$(sacct --format=state -n -P -j "${jobarr[0]}" | sort | uniq)
      if [ "$checkstat" = "COMPLETED" ]; then
        local jobarr=("${jobarr[@]:1}")
        continue
      elif [[ $checkstat =~ TIMEOUT|FAILED|CANCELLED ]]; then
        local stat="FAILED"
        exit 1 # throw non-zero exit instead of continue
      fi
    else
      local stat="COMPLETE"
      continue
    fi
    sleep $delay
  done
}

<<<<<<< HEAD
# print the command and it's parameters (useful for logging)
=======
>>>>>>> xchip_revert
function call {
  # local scopes variable to within this function
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

# General usage statement
function usage { 
  echo "Usage: sbatch xchip_scheduler.sh in the directory where you want the results output.
  -m mode -- select a mode for optimizing parameters. choices: atac, chip, cutNrun
  -i path to the project directory containing sample_ directories with fastq files (full path).
  -o destination directory -- corresponds to the project directory in the analysis branch.
  -n name -- sample name that matches all samples (e.g. Sample_KL).
  -g genome -- hg38, mm10 or rn6.
  -a annotation -- if hg38 or mm10 then gencode, otherwise ensembl
  -r groups -- a space delimited text file assigning each sample to a group  
  -f filter -- does the fastq file contain reads that failed illumina chastity fitler? Default: FALSE
  -l aligner -- which aligner to use. Default: bt2
  -p peak caller -- either genrich or macs2. Default: genrich
  -e email -- Default: hssgenomics.at.gmail.dot.com
  -h help -- prints this usage message." 
  exit 1
}

# Variables
MODE=""     # no default...must select a mode
IN=""       # No default, script fails if full path not assigned
OUT=""      # Destination directory (should be corresponding analysis dir)
NAME=""     # The name common to all samples in the experiment
GENOME=""   # The genome build that that the samples were aligned to
ANNO=""     # default should probably be gencode...
GROUP=""    # grouping, no default
ALIGN="bt2" # default aligner is bowtie2
PF="FALSE"  # does fastq include "Y" reads? Default: FALSE
PEAK="genrich" # default to genrich (until macs2 dies in hell)
EMAIL="hssgenomics@gmail.com" # default to hssgenomics email

#
# Parse command-line arguments
#
while getopts :m:i:o:n:g:a:r:f:l:p:e:h: opt; do
  case $opt in
    m)
      MODE=$OPTARG
      ;;
    i)
      IN=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    n)
      NAME=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    a)
      ANNO=$OPTARG
      ;;
    r)
      GROUP=$OPTARG
      ;;
    f)
      PF=$OPTARG
      ;;
    l)
      ALIGN=$OPTARG
      ;;
    p)
      PEAK=$OPTARG
      ;;
    e)
      EMAIL=$OPTARG
      ;;
    h)
      usage
      ;;
    *) 
      usage
      ;;
  esac
done

# set common variables (do not change with samples)
THREADS=6 # these are testing parameters...might need adjusting
MEMFREE=16 # ^^^
GENOMELOC=/athena/hssgenomics/scratch/genomes_2/$GENOME/Genome/$ALIGN/
ANNOLOC=/athena/hssgenomics/scratch/genomes_2/$GENOME/Annotation/${ANNO}.gtf
BLACKLIST=/athena/hssgenomics/scratch/genomes_2/$GENOME/Annotation/blacklist.bed

# Check validity of inputs
if [ ! -d "$IN" ] || [ "$IN" == "" ]; then
  printf "Invalid path to fastq directory. \n"
  usage
elif [ "$(find "$IN" -maxdepth 1 -name "$NAME*" 2>/dev/null | wc -l)" == 0 ] || [ "$NAME" == "" ]; then
  printf "Invalid name argument. \n"
  usage
elif [ "$NAME" == "" ]; then
  printf "Invalid Sample ID. \n"
  usage
elif [[ ! $GENOME =~ hg38|mm10|rn6 ]] || [ ! -d "$GENOMELOC" ]; then
  printf "Invalid genome selected (only mm10, rn6, and hg38 are currently supported). \n"
  usage
elif [ "$ANNO" == "" ] || [ ! -f "$ANNOLOC" ]; then
  printf "Annotation, %s, not a file or is not specified. \n" $ANNO
  usage
elif [[ ! $ALIGN =~ bt2|bwa ]]; then
  printf "Incorrect aligner specified, %s is not a supported aligner. Only bt2 and bwa are supported. \n" $ALIGN
  usage
elif [[ ! $MODE =~ atac|chip|cutNrun ]]; then
  printf "Incompatible mode selected. %s is not a supported mode \n" $MODE
  usage
elif [[ ! $PEAK =~ genrich|macs2 ]]; then
  printf "Incorrect peak caller selected. %s is not currently available. \n" $PEAK
  usage
elif [ ! -f "$GROUP" ]; then
  printf "%s is not a file. Create a space separated group file. \n" $GROUP
  usage
fi

###
##
# Here we run analyses for each sample (e.g. pre-processing)
##
###

call "$0" "$@" > ${OUT}/rerun_pipeline.sh
# in order to fork in runalign.sh we need access to the fastq files
# if these are in $TMPDIR they cannot be seen by other nodes
# so we make tmp in the cwd and bind it to GLOBALTMP
mkdir tmp
GLOBALTMP=$(readlink -f ./tmp)

# copy the genome in to the tmp dir
# cpGENOME="./scripts/sample/cpgenome.sh -s $GENOMELOC -t \$TMPDIR/genomeIdx"
# find the directories that match $NAME in the $IN directory
mapfile -t SAMPLEDIRS < <(find "$IN" -type d -name "${NAME}*")
# for each directory found:
for (( i=0; i<${#SAMPLEDIRS[*]}; i++ )); do
  # get the sample name
  SAMPLENAME=$(basename "${SAMPLEDIRS[$i]}")
  # create the output name
  ID=${SAMPLENAME}_${ALIGN}_${PEAK} # anything starting with ID is part of the final output
  echo "$ID" # print something so we can track it
  # check if current sample directory contains paired end reads
  ISPE=$(ls ${SAMPLEDIRS[$i]}/*R2*.fastq.gz 2>/dev/null | wc -l)
  # if the current sample directory contains R2 reads then do paired end things
  if [ "$ISPE" -gt 0 ]; then
    ISPE="pe"
    cpFASTQ="zcat ${SAMPLEDIRS[$i]}/*R1*.fastq.gz > \$TMPDIR/${SAMPLENAME}.R1.merged; \
             zcat ${SAMPLEDIRS[$i]}/*R2*.fastq.gz > \$TMPDIR/${SAMPLENAME}.R2.merged"
  else
    ISPE="se"
    cpFASTQ="zcat ${SAMPLEDIRS[$i]}/*.fastq.gz > \$TMPDIR/${SAMPLENAME}.merged"
  fi
  # run pass filter filtering ... e.g. illumina chastity fail
  # consider swapping this for a samtools filter after alignment...
  pfFILTER="./scripts/sample/pfilter.sh -p $PF -d \$TMPDIR"
  # fastp now runs from the qcfastp.sh script, modify that script as necessary
  qcFASTP="./scripts/sample/qcfastp.sh -e $ISPE -t $THREADS -d \$TMPDIR -s $SAMPLENAME -i $ID"
  # -g \$TMPDIR/genomeIdx/$GENOME  <- works with a single instance
  # -g $GENOMELOC/$GENOME <- works from any location (don't use with STAR)
  runALIGN="./scripts/sample/runalign.sh -e $ISPE -a $ALIGN -t $THREADS \
            -g ${GENOMELOC}/${GENOME} -d $GLOBALTMP -s $SAMPLENAME -i $ID -o \$TMPDIR"
  filterMC="./scripts/sample/filtermc.sh -t $THREADS -d \$TMPDIR -s $SAMPLENAME -i $ID"
  # remove reads aligning to blacklisted regions
  rmBLACK="./scripts/sample/rmblack.sh -b $BLACKLIST -d \$TMPDIR -s $SAMPLENAME -i $ID"
  # create a final index
  # generate some useful stats
  genSTAT="./scripts/sample/samstat.sh -t $THREADS -d \$TMPDIR -s $SAMPLENAME -i $ID"          
  # collect/clean after sample processing. this moves data from $TMPDIR to $OUT
  smplCLEAN="./scripts/sample/samplecleanup.sh -d \$TMPDIR -i $ID -o $OUT"
  # build each sbatch script and submit
  # note that emails have an @ in them...
  sed '
s@__JOB_NAME__@'"$SAMPLENAME"'@
s@__THREADS__@'"$THREADS"'@
s@__MEMFREE__@'"$MEMFREE"'@
s/__EMAIL__/'"$EMAIL"'/
s@__doCpGenome__@'"$cpGENOME"'@
s@__doCpFastq__@'"$cpFASTQ"'@
s@__doPfFilter__@'"$pfFILTER"'@
s@__doQcFastp__@'"$qcFASTP"'@
s@__doRunAlign__@'"$runALIGN"'@
s@__doFilterMc__@'"$filterMC"'@
s@__doRmBlack__@'"$rmBLACK"'@
s@__doGenStat__@'"$genSTAT"'@
s@__doSmplClean__@'"$smplCLEAN"'@
' slurmSample.template > ${ID}_slurm_script.sh  
s@__runGenericCommand__@'"$cpGENOME$cpFASTQ \&\& $pfFILTER \&\& $qcFASTP \&\& $fqSPLIT \&\& $cpLOGS"'@
' header.tmp > ${ID}_sample_slurm.sh'
  # sumbit the script via sbatch
  if [ "$JOBIDS" == "" ]; then
    JOBIDS=$(sbatch ${ID}_slurm_script.sh | cut -d' ' -f4) # Submitted batch job ->JOBID<-
  else
    JOBIDS=$JOBIDS:$(sbatch ${ID}_slurm_script.sh | cut -d' ' -f4)
  fi
  sleep 2
done

###
##
# Now we can run any analysis with groups of data (e.g. replicates)
##
###

### NOTE this is where the MODE parameter is used, up till now processing is identical

# collect groups of unique data from grouping table
GRPS=($(cut -d' ' -f 2 "$GROUP" | sort | uniq)) 
# for each group of samples
for (( j=0; j<${#GRPS[@]}; j++ )); do 
  # attempting to maintain symetry to the sample portion of the pipeline
  GROUPNAME=${GRPS[$j]}
  # we overwrite ID here (bad idea?)
  ID=${GROUPNAME}_${PEAK}
  GROUPTREAT=() # rewrite for each group
  GROUPCNTRL=() # also overwrite
  # this reads all lines of $GROUP, not just current group ${GRPS[j]} lines
  while read -r sample grouping condition; do 
    # if this is the current group
    if [ "$grouping" = "${GROUPNAME}" ]; then
      # check if it is TREAT or CNTRL
      if [ "$condition" = "TREAT" ]; then
        # since group.txt file was created on $sample names, add $ALIGN to get OUT dir
        GROUPTREAT+=("${sample}_${ALIGN}") # append the sample to group treatment array
      elif [ "$condition" = "CNTRL" ]; then
        GROUPCNTRL+=("${sample}_${ALIGN}") # append the sample to group control array
      fi
    fi
    # check anywhere in $GROUP for global BCKGRND
    if [ "$condition" = "BCKGRND" ]; then
      GROUPCNTRL+=("${sample}_${ALIGN}") # append the sample to group control array
    fi
  done < "$GROUP"

  # make a directory for grouped data
  mkGRPDIR="mkdir -p $OUT/Group_${GRPS[j]}"
  GRPOUT="$OUT/Group_${GRPS[j]}"
  # not first use of MODE designation (all previous steps the same for all modes)
  # also note different structure to args here. this allows us to pass an array
  # of folders to -t without confusion
  callPEAKS="./scripts/group/callpeaks.sh -m=$MODE -p=$PEAK -t=\"${GROUPTREAT[*]}\" \
           -b=$GROUPCNTRL -d=$GRPOUT -i=$ID"

  # grpCLEAN="./scripts/group/"

# these jobs are single threaded?
  sed '
s@__JOB_NAME__@'"$NAME"'@
s@__THREADS__@4@
s@__MEMFREE__@16@
s/__EMAIL__/'"$EMAIL"'/
s@__doMkOut__@'"$mkGRPDIR"'@
s@__doCallPeaks__@'"$callPEAKS"'@
' slurmGroup.template > ${ID}_slurm_script.sh
  if [ "$GRPJOBIDS" == "" ]; then
    # here we wait for JOBIDS thumbs up to run GRPJOBIDS
    GRPJOBIDS=$(sbatch --dependency=afterok:${JOBIDS} ${ID}_slurm_script.sh | cut -d' ' -f4)
  else
    GRPJOBIDS=$GRPJOBIDS:$(sbatch --dependency=afterok:${JOBIDS} ${ID}_slurm_script.sh | cut -d' ' -f4)
  fi
  sleep 2
done 

###
##
# Finally we run analyses which require all the data to be processed
##
###

# chkQUALITY="source /athena/hssgenomics/scratch/programs/MultiQC/bin/activate;\
#             multiqc $OUT; \
#             deactivate"

# cntPEAKREADS="Rscript --vanilla ${MCM} ${OUT}"

#doCOLLECT="mkdir -p $TOPDIR; mv ${OUT}/* ${TOPDIR}/"
#
#doCLEAN="ls ${TOPDIR}/*.sh | tar -czvf ${TOPDIR}/run_scripts.tar.gz -T -; \
#ls ${TOPDIR}/*.out | tar -czvf ${TOPDIR}/run_info.tar.gz -T -; \
#tar -czvf ${TOPDIR}/multiqc_data.tar.gz ${TOPDIR}/multiqc_data/; \
#rm $TOPDIR/*.sh; rm $TOPDIR/*.out; rm -r $TOPDIR/multiqc_data/"
## rm -rf $INDIRPATH/tmp
#
#sed '
#s@__JOB_NAME__@'"$NAME"'@
#s@__THREADS__@4@
#s@__MEMFREE__@16@
#s/__EMAIL__/'"$EMAIL"'/
#s@__doQuality__@'"$doQUALITY"'@
#s@__doCount__@'"$doCOUNT"'@
#s@__doCollect__@'"$doCOLLECT"'@
#s@__doClean__@'"$doCLEAN"'@
#' slurmGroup.template > ${GROUP}_slurm_script.sh 
#
#sbatch --dependency=afterok:${JOBIDS} ${NAME}_slurm_script.sh
#
# # here we submit one last job for experiment level analysis

call "$0" "$@" > $OUT/rerun_pipeline.sh

exit 0
