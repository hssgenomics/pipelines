# HSS Genomics ChIP/ATAC/Cut-n-Run Pipelines:

The transition to version control using Gitlab began last year 
but was never implemented beyond a useful method for backup.
This README will hopefully address that and provide guidelines for future pipeline maintenance.

## Before Running This Pipeline

See pipelines README to make sure you are all set up.

## What This Pipeline Does:

1. This pipeline is a full transition to a module approach.  
  - All pipeline steps exist as a bash script in the `scripts` directory.  
  - Each script (module) takes specific input, does one thing, and produces prescribed output (shocking I know).  
  - A direct consequence of this structure is that versions will exist within each module.  
  - Calling the pipeline with a specific version number will pass version parameter to each script and the script will call the closest matching version of that module (never newer than that version number).  
2. This pipeline performs the following steps:  
  1. Individual sample alignment  
  2. Peak calling with replicates and input (if available)  
  3. Motif validation using the appropriate motif as the target.  
    - ATAC -- CTCF and TSS (narrow)  
    - ChIP -- TSS (narrow) and supplied Motif  
    - Cut-N-Run -- TSS (broad)  
  4. Peak annotation  
3. This pipeline extends beyond single sample analysis.  
  - Previous pipelines performed single sample alignment and some small summary of the whole
experiment.  
  - This pipeline now performs start to finish processing by performing 3 tiers of computational tools.  
    1. slurmSample.template: All processing done on a single sample  
    2. slurmGroup.template: All processing performed using replicate information  
    3. slurmExperiment.template: All processing that uses full experiment information

## Example Execution

1. Make sure that all your fastq files are grouped into sample specific directories with a common starting name (e.g. Sample\_\*)  
2. Generate the grouping table.  
  - ls /path/to/data/dir > groups.txt
  - nano groups.txt
  - add a column for your groups (space separate the columns)
  - add a column for your controls (space separated)

```
Sample_F_DKO_ABC_OOO_OOO_Rep1 DKO_ABC TREAT
Sample_F_DKO_ABC_OOO_OOO_Rep2 DKO_ABC CNTRL
Sample_F_WT_ABC_OOO_OOO_Rep1 WT_ABC TREAT
Sample_M_DKO_ABC_OOO_OOO_Rep1 DKO_ABC TREAT
Sample_M_DKO_ABC_OOO_OOO_Rep2 DKO_ABC CNTRL
Sample_YAA_DKO_ABC_OOO_OOO_Rep1 YAA_DKO_ABC TREAT
Sample_YAA_DKO_ABC_OOO_OOO_Rep2 YAA_DKO_ABC CNTRL
```
  - In the above example, a control IP is performed for (almost) all groups. While the 2nd column can be free form (no spaces), the 3rd column must contain `TREAT` or `CNTRL` (as above) or `TREAT` `BCKGRND` (see example below) to indicate a treatment or a control group.
    - Note: `BCKGRND` is reserved for a "global" input or control IP (passed as 'background' to peak caller for all groups). 
```
Sample_F_DKO_ABC_OOO_OOO_Rep1 FDKO_ABC TREAT
Sample_F_DKO_ABC_OOO_OOO_Rep2 FDKO_ABC TREAT
Sample_F_WT_ABC_OOO_OOO_Rep1 WT_ABC BCKGRND
Sample_M_DKO_ABC_OOO_OOO_Rep1 MDKO_ABC TREAT
Sample_M_DKO_ABC_OOO_OOO_Rep2 MDKO_ABC TREAT
Sample_YAA_DKO_ABC_OOO_OOO_Rep1 YAA_DKO_ABC TREAT
Sample_YAA_DKO_ABC_OOO_OOO_Rep2 YAA_DKO_ABC TREAT
```















