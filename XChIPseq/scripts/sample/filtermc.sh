#!/bin/bash
# set -x

# Expected Input:
# # expects an aligned and de-duplicated bam file 

# Expected Behavior:
# # removes chrM and preserves some stats

# Expected output:
# # a bam file with reads aligning to chrM removed

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -lt 10 ]; then
  call "$0" "$@"
  usage
fi

while getopts :t:s:i:d:o: opt; do
  case $opt in
    t)
      THREADS=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# consider double checking parameters here.
# should all be checked by the parent script.

# create the index for a sample
samtools index -@ $THREADS $DIR/${SAMPLE}.aligned.nodup.bam
samtools idxstats --threads $THREADS $DIR/${SAMPLE}.aligned.nodup.bam > ${OUT}/${ID}.idxstat.log
# calculate the stats e.g. which reads align where...
samtools idxstats --threads $THREADS $DIR/${SAMPLE}.aligned.nodup.bam | \
  cut -f 1 | grep -v 'chrM' | \
  xargs samtools view --threads $THREADS -hb $DIR/${SAMPLE}.aligned.nodup.bam > \
    ${OUT}/${SAMPLE}.aligned.nodup.noM.bam

exit 0
