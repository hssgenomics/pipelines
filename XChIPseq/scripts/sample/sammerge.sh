#!/bin/bash
# set -x

# Expected Input:
# # expects directory containing multiple sam files which need to be merged

# Expected Behavior:
# # convert multiple sam files into a single sorted BAM 

# Expected output:
# # single sorted (possibly de-duplicated) bam file 

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -lt 14 ]; then
  call "$0" "$@"
  usage
fi

while getopts :e:t:s:i:p:d:o: opt; do
  case $opt in
    e)
      END=$OPTARG
      ;;
    t)
      THREADS=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    p)
      TDIR=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# NOTE -p option is tmp directory for intermediate samtools sorting files...

# now put the pieces back together
if [ "$END" = "pe" ]; then
  # all sam files in the $DIR need to be merged...
  # not that find executes merge with all files found and sends the results to stdout
  find $DIR/ -name "${SAMPLE}*.sam" -exec samtools merge --threads $THREADS - {} + | \
    samtools sort -n --threads $THREADS -T ${TDIR}/${SAMPLE}.samtools.nsorting - | \
    samtools fixmate --threads $THREADS -rm - - | \
    samtools sort --threads $THREADS -T ${TDIR}/${SAMPLE}.samtools.psorting - | \
    samtools markdup --threads $THREADS -rs - ${OUT}/${SAMPLE}.aligned.nodup.bam \
    2> ${OUT}/${ID}.markdup.log
elif [ "$END" = "se" ]; then
  find $DIR/ -name "${SAMPLE}*.sam" -exec samtools merge --threads $THREADS - {} + | \
    samtools sort --threads $THREADS -T ${TDIR}/${SAMPLE}.samtools.psorting - -o ${OUT}/${SAMPLE}.aligned.nodup.bam
fi
 
exit 0
