#!/bin/bash
# set -x

# Expected Input:
# # a set of files produced by the pipeline

# Expected Behavior:
# # all files which start with the ID value are moved
# # into a ID directory and prepared for the next 
# # stage of the pipeline which works on groups of samples.

# Expected output:
# # a single directory in "OUT" called ID

function usage {
printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -lt 8 ]; then
  call "$0" "$@"
  usage
fi

while getopts :s:i:d:o: opt; do
  case $opt in
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# consider double checking parameters here.
# should all be checked by the parent script.

# not useful except for debugging
ls $DIR/* > file.manifest.log

mkdir -p ${OUT}/${ID}
# printf "$DIR/${ID}.* ${OUT}/${ID}/\n"
mv ${DIR}/${ID}.* ${OUT}/${ID}/

exit 0
