#!/bin/bash
# set -x

# Expected Input:
# # gzipped fastq files 

# Expected Behavior:
# # copy all gzipped fastq files to a single fastq file

# Expected output:
# # *.merged file that is actually a fastq

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -lt 8 ]; then
  call "$0" "$@"
  usage
fi

while getopts :s:i:d:o: opt; do
  case $opt in
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# consider double checking parameters here.
# should all be checked by the parent script.

ISPE=$(find $DIR -name "*R2*.fastq.gz" 2>/dev/null | wc -l)

if [ "$ISPE" -gt 0 ]; then
  zcat ${DIR}/*R1*.fastq.gz > ${OUT}/${SAMPLE}.R1.merged
  zcat ${DIR}/*R2*.fastq.gz > ${OUT}/${SAMPLE}.R2.merged
else
  zcat ${DIR}/*.fastq.gz > ${OUT}/${SAMPLE}.merged 
fi

# this is the first line written to the call log.
call "$0" "$@" > ${OUT}/${ID}.call.log

exit 0
