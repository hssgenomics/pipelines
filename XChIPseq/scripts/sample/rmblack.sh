#!/bin/bash
# set -x

# Expected Input:
# # a BAM file with duplicates and chrM removed
# # ending in *.aligned.nodup.noM.bam

# Expected Behavior:
# # remove reads that align to blacklisted regions 

# Expected output:
# # a BAM file with black-listed regions removed

function usage {
printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -lt 10 ]; then
  call "$0" "$@"
  usage
fi

while getopts :b:d:s:i:d: opt; do
  case $opt in
    b)
      BLACKLIST=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# consider double checking parameters here.
# should all be checked by the parent script.

# printf "running bedtools subtract with parameters\n-abam $DIR/${SAMPLE}.aligned.nodup.noM.bam\n-b $BLACKLIST\n-o $DIR/${ID}.aliogned.nodup.noM.noblack.bam"

bedtools subtract -A -abam $DIR/${SAMPLE}.aligned.nodup.noM.bam -b $BLACKLIST > ${OUT}/${SAMPLE}.aligned.nodup.noM.noblack.bam
 
exit 0
