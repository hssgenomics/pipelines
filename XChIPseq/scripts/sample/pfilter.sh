#!/bin/bash
# set -x

# Expected Input:
# # Merged Fastq files (possible gzipped? not tested).
# # Either 1 file (for single end) or 2 files for paired end.

# Expected Behavior:
# # If FILTER is FALSE then copy the file unchanged
# # if FILTER is TRUE then filter out reads that fail 
# # illumina chastity filter (aka :Y: reads)

# Expected output:
# # 1 (SE) or 2 (PE) fastq file ending in "*merged.fastq"

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -lt 10 ]; then
  call "$0" "$@"
  usage
fi

# FASTQ=()
while getopts :f:s:i:d:o: opt; do
  case $opt in
    f)
      FILTER=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# paired end and single end don't matter since files are translated directly

FASTQ=$(find $DIR -name "${SAMPLE}*.merged")

if [ "$FILTER" == "FALSE" ]; then
  # cp with parallel? I hear you ask. 
  # yes, but only because rename is not to be trusted.
  # https://images.app.goo.gl/dMGQ7VHpZGQCj4hk8
  parallel -k -j 0 "cp {} {}.fastq" ::: "${FASTQ[@]}"
elif [ "$FILTER" == "TRUE" ]; then
  parallel -k -j 0 "grep -A 3 '^@.* [^:]*:N:[^:]*:' {} | \
                    egrep -v '^\-\-$' > {}.fastq" ::: "${FASTQ[@]}"
else
  usage
fi

# append to the call log
call "$0" "$@" >> ${OUT}/${ID}.call.log

exit 0
