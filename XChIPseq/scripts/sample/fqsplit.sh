#!/bin/bash
# set -x

# Expected Input:
# # the output from fastp 

# Expected Behavior:
# # split the cleaned up fastq files and copy them back to a globally visible location

# Expected output:
# # splits fastq files in 15M reads each. writing them to a specified OUT dir

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -lt 10 ]; then
  call "$0" "$@"
  usage
fi

while getopts :r:s:i:d:o: opt; do
  case $opt in
    r)
      READS=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;; 
    *) 
      usage
      ;;
  esac
done

ISPE=$(find $DIR -name "${SAMPLE}*R2*" 2>/dev/null | wc -l)

if [ "$ISPE" -gt 0 ]; then
  # with bt2 --sensitive-local 60000000 takes about 30min 
  split -l $READS --additional-suffix=.fastq ${DIR}/${SAMPLE}.R1.merged.qc.fastq \
    ${OUT}/${SAMPLE}.R1.merged.qc. 
  split -l $READS --additional-suffix=.fastq ${DIR}/${SAMPLE}.R2.merged.qc.fastq \
    ${OUT}/${SAMPLE}.R2.merged.qc. 
else
  split -l $READS --additional-suffix=.fastq ${DIR}/${SAMPLE}.merged.qc.fastq \
    ${OUT}/${SAMPLE}.merged.qc.
fi

# this is awkward since we've been storing it in the $OUT dir up to this point
# now since we are writing files to a different dir we write here and copy later
call "$0" "$@" >> ${DIR}/${ID}.call.log 

exit 0
