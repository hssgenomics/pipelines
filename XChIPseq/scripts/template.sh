#!/bin/bash
# set -x

# Expected Input:
# # 

# Expected Behavior:
# # 

# Expected output:
# # 

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -lt 10 ]; then
  call "$0" "$@"
  usage
fi

while getopts :t:s:i:d:o: opt; do
  case $opt in
    t)
      THREADS=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# consider double checking parameters here.
# should all be checked by the parent script.

if [ "$VARA" == "TRUE" ]; then
  # do thing a 
elif [ "$VARA" == "FALSE" ]; then
  # do thing b
else
  usage
fi
 
exit 0
