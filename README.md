# HSS Genomics Pipelines:

The transition to version control using Gitlab began last year 
but was never implemented beyond a useful method for backup.
This README will hopefully address that and provide guidelines for future pipeline maintenance.

## Before you Run the Pipelines

1. To obtain `ssh` access to the cluster from your HSS machine, contact [Daniel Zoquier](zoquierd@HSS.EDU) with your port number (written above your ethernet port) and your location (including floor and office number).

2. Once you can access the WCM server update your ~/.bash\_profile by adding the following lines:  

```bash
        # prevent your account from locking other users out
        umask g+w

        # make sure R is using the globally available libraries
        export R_LIBS=/athena/hssgenomics/scratch/programs/R/3.4.0/
 
        # make sure hssgenomics programs are available in the path
        PATH=$PATH:/athena/hssgenomics/scratch/programs/bin

        # detect kernel then source spack file if it exists
        KERNEL=$(uname -r | cut -d '.' -f1,2)
        if [ "${KERNEL}" = "2.6" ] ; then
          if [ -f /pbtech_mounts/softlib001/apps/EL6/spack/share/spack/setup-env.sh ] ; then
            . /pbtech_mounts/softlib001/apps/EL6/spack/share/spack/setup-env.sh
          fi
        elif [ "${KERNEL}" = "3.10" ] ; then
          if [ -f /pbtech_mounts/softlib001/apps/EL7/spack/share/spack/setup-env.sh ] ; then
            . /pbtech_mounts/softlib001/apps/EL7/spack/share/spack/setup-env.sh
          fi
        fi
```  
3. `source .bash\_profile`
3. Test that everything is working. 
Move to the head node `ssh panda2.pbtech` and then to a working node `qrsh -l h_vmem=3G -l athena=true`. Now check that the following commands work without issue.  
  a. `spack load r@3.4.0`  
  b. `source /athena/hssgenomics/scratch/programs/MultiQC/bin/activate; deactivate`  
  c. `which tn5Adjust`

## Using Git for Pipeline Maintenance

Using Git can be less than straightforward, hopefully the following walkthrough will help. 
For starters read [this quick walkthrough from Github](https://guides.github.com/introduction/flow/index.html).
In general, the master branch exists on gitlab and is always fully functional. 
Nothing gets added to the master branch until it has been tested and approved.
Any and all modifications should be made on your own branch. 
Once you have tested your changes submit a merge request for them to be merged into the master branch.
If everyone is able to successfully test the modifications and agrees that the modifications 
should be made then your changes will be merged with the master branch.

What this looks like programatically.

1. Download the most current version of the repository.  
  a. `mkdir hssgenomics/`  
  b. `git clone https://gitlab.com/hssgenomics/pipeline.git`  
  c. Enter your username and password


  You now have an up to date copy of the HSS Genomics pipeline in your hssgenomics directory.

2. Create a branch off of `dev` to start working on the pipeline  
  a. Switch to `dev` using `git checkout dev`  
  b. Create your branch with a meaningful name `git checkout -b requested_feature_99`  
  c. Make the necessary changes.  
  d. Add the changes to your local copy `git commit -a -m "descriptive message"`  
  e. Switch back to `dev` and pull your changes  
     - `git checkout dev`  
     - `git merge --no-ff requested_feature_99`  
  f. Push your local `dev` changes to the remote repository `git push origin dev`  


  Once `dev` has progressed to a new stable point, `dev` can be merged into `master`

3. Merge your changes  
  a. Go to https://gitlab.com/hssgenomics/pipeline/branches and submit a merge request for your branch  
  b. Once everyone has confirmed the changes the merge can be completed

