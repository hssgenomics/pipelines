#!/usr/bin/perl -l
use strict;
use warnings;
use Getopt::Long;
use File::Spec;
use File::Basename;
use Cwd;

# Set definite variables
my $bin		= dirname(File::Spec->rel2abs(__FILE__));
my $resultsdir  = cwd();
my @names       = ();        # Stores the whole directory path       

# Variables holding paths 
# my $fastqc              = "/softlib/exe/all/pkg/fastqc/0.10.1/java/fastqc";
my $chipseeqerdir	= "/athena/hssgenomics/scratch/programs/ChIPseeqer-2.1/dist";
my $markduplicates	= "/athena/hssgenomics/scratch/programs/picard-tools-1.85/MarkDuplicates.jar";
my $genome_dir		= "/athena/hssgenomics/scratch/genomes_2/";

# Variables inherited from command line
my $dir         = undef;	# Input directory
my $n_threads	= 8;		# Number of available threads
my $memory_pt	= "12G";	# Maximum memory available
my $genome      = undef;	# Genome build?
my $aligner	= "bt2";	# optional bt2 or bwa
my $email	= "";		# Default email


# Placeholding variables
my $name    = undef;    # Stores the basename of the directory for output file names
my $cmd1_1    = undef;    # Rsync genome.fa to tmp
my $cmd1_2    = undef;    # Rsync aligner index to tmp
my $cmd1_3    = undef;    # Merge fastq and move to tmp
my $cmd2_1    = undef;    # Remove reads failing to pass illumina filter
my $cmd2_2    = undef;    # Run trim_galore command
my $cmd3_1    = undef;    # Run aligner (bt2 or bwa)
my $cmd3_2    = undef;    # Run bwa samse
my $cmd4_1    = undef;    # Samtools filtering
my $cmd4_2    = undef;    # adjust tn5 offsets
my $cmd4_3    = undef;    # Samtools sort
my $cmd4_4    = undef;    # samtools index
my $cmd4_5    = undef;    # Remove chrM
my $cmd4_6    = undef;    # Remove blacklist
my $cmd4_7    = undef;    # Remove duplicates
my $cmd4_8    = undef;    # Samtools index
my $cmd5_1    = undef;    # Split reads command
my $cmd5_2    = undef;    # Estimate number of clonal reads
my $cmd5_3    = undef;    # Make reads track for UCSC Genome Browser
my $cmd6_1    = undef;    # check TMPDIR contents
my $cmd6_2    = undef;    # copy READS to results dir
my $cmd6_3    = undef;    # copy log/fastqc files to results dir
my $cmd6_4    = undef;    # copy bam files to results dir

if (@ARGV == 0) {
    die "Lack of arguments. Run script as: atacseq_alignment_SE.pl\n
	--dir		= Input directory.
	--n_threads	= Number of available threads. Default is 8.
	--memory_pt	= Maximum memory available. Default is 16.
	--genome	= Genome build, e.g., hg19.
	--aligner	= Aligner choice bwa or bt2. Default is bt2
	--email		= Email to send job alerts.";
}

# Read options
GetOptions(
"dir=s"		=> \$dir,
"n_threads=s"	=> \$n_threads,
"memory_pt=s"	=> \$memory_pt,
"genome=s"	=> \$genome,
"aligner=s"	=> \$aligner); # ,
# "email=s"	=> \$email);

# Store prefix for output files
@names		= split('/',$dir);
$name 		= $names[-1];
$resultsdir	.= "/".$name."_OUT";

# Create results directory
unless(mkdir $resultsdir) {
         die "Unable to create $resultsdir\n";
}

# Select appropriate genome sequence
my $genome_index	= "$genome_dir/$genome/Genome/$aligner/";
my $genome_fasta    	= "$genome_dir/$genome/Genome/genome.fa";                                     
my $blacklist           = "$genome_dir/$genome/Annotation/blacklist.bed";

# ---------------
# start bash file modified for slurm
# ---------------
print "#!/bin/bash -l";
#print "#\$ -N $name";
#print "#\$ -pe smp $n_threads";
#print "#\$ -j y";
#print "#\$ -cwd";
#print "#\$ -l h_vmem=$memory_pt";
#print "#\$ -m ae";
#print "#\$ -l athena=true";
# print "#\$ -M $email";

print "#SBATCH --partition=panda";
print "#SBATCH --ntasks=1 --cpus-per-task=$n_threads";
print "#SBATCH --job-name=$name"."CHIP_aln";
print "#SBATCH --time=24:00:00";   # HH/MM/SS
print "#SBATCH --mem=$memory_pt";
print "#SBATCH --mail-user=$email";
print "#SBATCH --mail-type=END,FAIL";
# new requirement for SLURM
print "source ~/.bashrc";

print "# general function that exits after printing its text argument";
print "# in a standard format which can be easily grep'd.";
print "err() {";
print "  echo \"\$1...exiting\"";
print "  exit 1 # any non-0 exit code signals an error";
print "}";
print "\n";
print "# function to check return code of programs.";
print "# exits with standard message if code is non-zero;";
print "# otherwise displays completiong message and date.";
print "#   arg1 is the return code (usually $?)";
print "#   arg2 is text describing what ran";
print "ckRes() {";
print "  if [ \"\$1\" == \"0\" ]; then";
print "    echo \"..Done \$2 `date`\"";
print "	 else";
print "	   err \"\$2 returned non-0 exit code \$1\"";
print "	 fi";
print "}";
print "\n\n";

# load in necessary libraries and source directories
#jdk??? whay do we load this

print "spack load parallel; spack load perl\@5.24.1/hnx5wqj; spack load zlib\@1.2.11/3m5jwvc; spack load samtools\@1.8; spack load bedtools2\@2.27.0; spack load bowtie2\@2.3.4.1";

#
# Step 1: (1) Copy genome.fa, (2) genome index, and (3) sample fastq to current tmp dir
#
$cmd1_1		= "rsync -Lp $genome_fasta \$TMPDIR";
print "echo STEP 1.1: Copy genome.fa file to tmp \n";
print "time $cmd1_1 \n";
print "ckRes \$? \"rsync genome.fa to tmp\";";

$cmd1_2	= "rsync -rLp $genome_index/* \$TMPDIR";
print "echo STEP 1.2: Copy aligner index folder to tmp \n";
print "time $cmd1_2 \n";
print "ckRes \$? \"rsync aligner index folder to tmp\";";

$genome_index    = "\$TMPDIR/$genome";
$genome_fasta    = "\$TMPDIR/genome.fa";

$cmd1_3    = "cat $dir/*.fastq.gz > \$TMPDIR/$name.merged.gz";
print "echo STEP 1.3: Merging and moving fastq files \n";
print "time $cmd1_3 \n";
print "ckRes \$? \"cat\";";

#
# Step 2: Preprocessing
# (1) Keep only reads that pass Illumina filters (PF reads)
# (2) Trim adapters and low quality (<20) bases and run FastQC
#
$cmd2_1    = "zcat \$TMPDIR/$name.merged.gz | grep -A 3 '^@.* [^:]*:N:[^:]*:' | egrep -v '^\\-\\-\$\' > \$TMPDIR/$name.merged.PF.fastq";
print "echo STEP 2.1: Preprocessing reads - PF Illumina filter";
print "time $cmd2_1 \n";
print "ckRes \$? \"PF reads processing\";";

$cmd2_2    = "trim_galore --output_dir \$TMPDIR/ --fastqc \$TMPDIR/$name.merged.PF.fastq";
print "echo STEP 2.2: Preprocess and QC reads -- trim_galore/fastqc";
print "time $cmd2_2 \n";
print "ckRes \$? \"PF reads processing --trim_galore/fastqc\";";

#
# Step 3: (1) Align FASTQ files with bwa aln (2) Convert aligned reads to sam with bwa samse
#
# TODO: bwa aln is good for < 70bp reads but bwa mem should be used for > 70bp
if ($aligner eq "bwa") {
    $cmd3_1    = "bwa aln -t $n_threads $genome_fasta \$TMPDIR/$name.merged.PF_trimmed.fq > \$TMPDIR/$name.merged.PF_trimmed.fq.sai";
    print "echo STEP 3.1: bwa aln -- aligning reads to $genome \n";
    print "time $cmd3_1 \n";
    print "ckRes \$? \"bwa aln -- aligning reads to $genome\";";

    $cmd3_2    = "bwa samse $genome_fasta \$TMPDIR/$name.merged.PF_trimmed.fq.sai \$TMPDIR/$name.merged.PF_trimmed.fq > \$TMPDIR/$name.merged.PF_trimmed.sam";
    print "echo STEP 3.2: bwa samse -- generating sam files \n";
    print "time $cmd3_2 \n";
    print "ckRes \$? \"bwa samse -- generating sam files\";";

} elsif ($aligner eq "bt2") {
    $cmd3_1    = "bowtie2 --local -q -p $n_threads -x $genome_index -U \$TMPDIR/$name.merged.PF_trimmed.fq -S \$TMPDIR/$name.merged.PF_trimmed.sam 2> \$TMPDIR/$name.bowtie2.txt";
    print "echo STEP 3.1: bowtie2 -- aligning reads to $genome \n";
    print "time $cmd3_1 \n";
    print "ckRes \$? \"bowtie2 -- aligning reads to $genome\";";
}

#
# Step 4: (1) Filter, sort and convert sam to bam (2) index sorted bam files (3) remove chrM (4) remove blacklisted regions (5) remove PCR duplicates (6) regenerate index 
#
$cmd4_1    = "samtools view -uhb -q 1 -F 2820 -@ $n_threads \$TMPDIR/$name.merged.PF_trimmed.sam | samtools sort -@ $n_threads -o \$TMPDIR/$name.aligned.bam -";
print "echo STEP 4.1: samtools filter poor quality alignments. \n";
print "time $cmd4_1 \n";
print "ckRes \$? \"samtools filter poor quality alignments.\";";

$cmd4_2    = "java -Xmx12g -jar $markduplicates REMOVE_DUPLICATES=TRUE INPUT=\$TMPDIR/$name.aligned.bam METRICS_FILE=\$TMPDIR/$name.dedup.txt OUTPUT=\$TMPDIR/$name.aligned.nodup.bam VALIDATION_STRINGENCY=LENIENT ASSUME_SORTED=TRUE";
print "echo STEP 4.2: PicardTools remove duplicates \n";
print "time $cmd4_2 \n";
print "ckRes \$? \"PicardTools remove duplicates\";";

#$cmd4_3    = "tn5Adjust \$TMPDIR/$name.aligned.nodup.bam $n_threads";
#print "echo STEP 4.3: Adjust reads by Tn5 offsets \n";
#print "time $cmd4_3 \n";
#print "ckRes \$? \"Adjust reads by Tn5 offsets\";";

$cmd4_4    = "samtools sort -@ $n_threads -o \$TMPDIR/$name.aligned.nodup.sorted.bam \$TMPDIR/$name.aligned.nodup.bam";
print "echo STEP 4.4: Samtools sort \n";
print "time $cmd4_4 \n";
print "ckRes \$? \"Samtools sort\";";

$cmd4_5    = "samtools index \$TMPDIR/$name.aligned.nodup.sorted.bam";
print "echo STEP 4.5: samtools index \n";
print "time $cmd4_5 \n";
print "ckRes \$? \"samtools index\";";

$cmd4_6    = "samtools idxstats \$TMPDIR/$name.aligned.nodup.sorted.bam | cut -f 1 | grep -v chrM | xargs samtools view -hb \$TMPDIR/$name.aligned.nodup.sorted.bam > \$TMPDIR/$name.aligned.nodup.sorted.noM.bam";
print "echo STEP 4.6: samtools remove chrM \n";
print "time $cmd4_6 \n";
print "ckRes \$? \"samtools remove chrM\";";

$cmd4_7    = "bedtools subtract -A -abam \$TMPDIR/$name.aligned.nodup.sorted.noM.bam -b $blacklist > \$TMPDIR/$name.aligned.nodup.sorted.noM.noblack.bam";
print "echo STEP 4.7: bedtools remove black regions \n";
print "time $cmd4_7 \n";
print "ckRes \$? \"bedtools remove black regions\";";

$cmd4_8    = "samtools index \$TMPDIR/$name.aligned.nodup.sorted.noM.noblack.bam";
print "echo STEP 4.8: samtools regen index \n";
print "time $cmd4_8 \n";
print "ckRes \$? \"samtools regen index\";";

#
# Step 5: ChIPseeqer (1) Split reads (2) estimate number of clonal reads (3) make ucsc tracks 
#
#$cmd5_1		= "$chipseeqerdir/ChIPseeqerSplitReadFiles --format=bam --files=\$TMPDIR/$name.aligned.nodup.sorted.noM.noblack.bam --outputfolder=\$TMPDIR/READS";
#print "echo STEP 5.1: Splitting reads \n";
#print "time $cmd5_1 \n";
#print "ckRes \$? \"Splitting reads\";";

#$cmd5_2		= "$chipseeqerdir/ChIPseeqerGetNumClonalReads -chipdir \$TMPDIR/READS -format bam -chrdata $chipseeqerdir/DATA/$genome.chrdata > \$TMPDIR/$name.ClonalReads.txt";
#print "echo STEP 5.2: Estimate clonal reads \n";
#print "time $cmd5_2 \n";
#print "ckRes \$? \"Estimate clonal reads\";";

#$cmd5_3		= "$chipseeqerdir/ChIPseeqerMakeReadDensityTrack --readdir=\$TMPDIR/READS --format=bam --chrdata=$chipseeqerdir/DATA/$genome.chrdata  --trackname=$name --bigwig=1";
#print "echo STEP 5.3: Make reads track for UCSC Genome Browser \n";
#print "time $cmd5_3 \n";
#print "ckRes \$? \"Make reads track for UCSC Genome Browser\";";

#
# Step 6: (1) Remove intermediate files (2) Copy files to results directory
#
$cmd6_1        ="ls \$TMPDIR/*";
print "echo STEP 6.1: Checking \$TMPDIR contents \n";
print "time $cmd6_1 \n";
print "ckRes \$? \"Checking \$TMPDIR contents\";";

#$cmd6_2        ="cp -rv \$TMPDIR/READS $resultsdir";
#print "echo STEP 6.2: Copy UCSC tracks to $resultsdir \n";
#print "time $cmd6_2 \n";
#print "ckRes \$? \"Copy UCSC tracks to $resultsdir\";";

$cmd6_3        ="cp -v \$TMPDIR/*txt $resultsdir && cp -rv \$TMPDIR/*fastqc* $resultsdir";
print "echo STEP 6.3: Copy log files to $resultsdir \n";
print "time $cmd6_3 \n";
print "ckRes \$? \"Copy log files to $resultsdir\";";

$cmd6_4        ="cp -v \$TMPDIR/$name.aligned.nodup.sorted.noM.noblack.bam* $resultsdir";
print "echo STEP 6.4: Copy bam files to $resultsdir \n";
print "time $cmd6_4 \n";
print "ckRes \$? \"Copy bam files to $resultsdir\";";

print "echo Pipeline completed without errors. Exiting..."; 
print "exit 0";
