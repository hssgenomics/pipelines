#!/bin/bash -l 
#$ -N ChIP-seq_Summary 
#$ -pe smp 6
#$ -j y
#$ -cwd
#$ -l h_vmem=8G
#$ -m ae
#$ -l athena=true
#$ -M doliv071@gmail.com

#####################################################################
# Summarization script for aggregating all the results from a single 
# chip-seq experiment and generating summary stats including 
# QC (multiqc), peak calling, and sample correlation
#####################################################################

#
# Functions
#
err() {
  echo "$1...exiting"
  exit 1 # any non-0 exit code signals an error";
}

# function to check return code of programs.";
# exits with standard message if code is non-zero;";
# otherwise displays completiong message and date.";
#   arg1 is the return code (usually $?)";
#   arg2 is text describing what ran";
ckRes() {
  if [ "$1" == "0" ]; then
    echo "..Done $2 `date`"
  else
    err "$2 returned non-0 exit code $1"
  fi
}

# General usage statement
usage() { 
  echo "Usage: Run this script (atacseq_summarize.sh) in the directory where you want the results ouptut to with the following args.
  -d path to the sample directory (use full path to the directory)
  -n name -- sample name that matches all samples (e.g. Sample_KL)
  -i input -- the input (control) sample (optional).
  -g genome build -- hg19, hg38 (default), mm9, mm10
  -a annotation -- gencode (default), igenomes, refseq
  -e email -- user's email (do not use hss.edu)
  -h help -- prints this usage message" 
  exit 1
}

#
# Variables
#
INDIRPATH=""    # Default dir is the current working directory
NAME=""		   # The name common to all samples in the experiment
INPUT=""	   # Optional parameter for input control sample
GENOME="hg38"      # The genome build that that the samples were aligned to
ANNO="gencode"     # The annotation that was used to count features
EMAIL=""

#
# Parse command-line arguments
#

##########################################################################
# IN DEV: ADD PARAMETER FOR BANDWIDTH IN MACS2
##########################################################################

while getopts :d:n:i:g:a:e:h: opt; do
  case $opt in
    d)
      INDIRPATH=$OPTARG
      ;;
    n)
      NAME=$OPTARG
      ;;
    i)
      INPUT=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    a)
      ANNO=$OPTARG
      ;;
    e)
      EMAIL=$OPTARG
      ;;
    h)
      usage
      ;;
    *) 
      usage
      ;;
  esac
done

#
# Check validity of inputs
#
if [ ! -d $INDIRPATH ] || [ "$INDIRPATH" == "" ]; then
  echo "Invalid input directory."
  usage
elif [ `find . -maxdepth 1 -name "$NAME*" 2>/dev/null | wc -l` == 0 ] || [ "$NAME" == "" ]; then
  echo "Invalid name argument."
  usage
elif [[ ! $GENOME =~ hg18|hg19|hg38|mm9|mm10 ]]; then
  echo "Invalid genome build selected."
  usage
elif [ "$EMAIL" == "" ]; then
  echo "Invalid email address, please enter a valid email."
  usage
fi

# set variables and load packages
TOPDIR=$INDIRPATH/$NAME
SAMPCORR=/athena/hssgenomics/scratch/programs/R/detools/ATACSampleCorr.r
GENOMEMAP="/athena/hssgenomics/scratch/programs/ChIPseeqer-2.1/data/${GENOME}.chrdata"

#
# Step 1: aggregate samples for summarization
#
TOCOPY=`find $INDIRPATH -maxdepth 1 -name "$NAME*"`
SHCOPY=`find $INDIRPATH -maxdepth 1 -name "*$NAME*.sh"`
echo "creating $TOPDIR and moving $TOCOPY files there..."
mkdir $TOPDIR; cd $TOPDIR; mv $TOCOPY $TOPDIR; mv $SHCOPY $TOPDIR

#
# Step 2: Call peaks with macs2
#
spack load -r r@3.4.0 py-macs2@2.1.1.20160309 bedtools2@2.26.0 python@3.6.0
mkdir PEAKS; cd PEAKS
GENOMESIZE=`awk '{ map = $2 * $3 ; tot += map ; print tot }' $GENOMEMAP | tail -n 1`
if [ ! "$INPUT" == "" ]; then
  TREAT=`find $TOPDIR -name "${NAME}*.bam" | grep -v $TOPDIR/$INPUT`
  CONTROL=`find $TOPDIR -name "${NAME}*.bam" | grep $TOPDIR/$INPUT`
  for i in $TREAT; do
    macs2 callpeak  --broad --bw 180 --keep-dup all --qvalue 0.05 -B -g $GENOMESIZE -t $i -c $CONTROL -n `basename ${i%.aligned*}` 
    ckRes $? "Calling peaks with MACS2."
  done
elif [ "$INPUT" == "" ]; then
  TREAT=`find $TOPDIR -name "${NAME}*.bam"`
  for i in $TREAT; do
    macs2 callpeak  --broad --bw 180 --keep-dup all --qvalue 0.05 -B -g $GENOMESIZE -t $i -n `basename ${i%.aligned*}`
    ckRes $? "Calling peaks with MACS2."
  done
fi

#
# Step 3: Correlation analysis. 
#    (1) generate GTF from peaks
#    (2) count reads in peaks
#    (3) calculate correlation coefficient
#
cat $(ls *.broadPeak) | cut -f 1,2,3,4 | sort -k 1,1 -k 2,2n | \
bedtools merge -d 50 -c 4 -o collapse -i - | cut -d ',' -f 1 | \
bedToGenePred stdin stdout | genePredToGtf file stdin stdout | \
tr '\000' '.' > ${NAME}.union.gtf
ckRes $? "Generating peaks GTF."

BAMFILES=`find $TOPDIR -name ${NAME}*.bam | tr '\n' ' '`
featureCounts -T 6 -t exon -g gene_id -a ${NAME}.union.gtf -o ${NAME}.counts.txt $BAMFILES
ckRes $? "Counting reads per feature."

Rscript --vanilla $SAMPCORR ${NAME}.counts.txt
ckRes $? "Calculating correltation coefficient."
module purge

#
# Step 4: run multiqc
#
source /athena/hssgenomics/scratch/programs/MultiQC/bin/activate
multiqc $TOPDIR # waiting for spack version
ckRes $? "Running MultiQC."
deactivate

echo "Summarization completed without errors. Exiting..."
exit 0
