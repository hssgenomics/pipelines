#!/bin/bash -l
#SBATCH --partition=panda
#SBATCH --ntasks=1 --cpus-per-task=12
#SBATCH --job-name=$name"."ATAC_aln
#SBATCH --time=24:00:00   # HH/MM/SS
#SBATCH --mem=60GB
#SBATCH --mail-user=$email
#SBATCH --mail-type=END,FAIL
# new requirement for SLURM
source ~/.bashrc


#####################################################################
# Summarization script for aggregating all the results from a single 
# atac-seq experiment and generating summary stats including 
# QC (multiqc), peak calling, overlay tracks, and sample correlation
#####################################################################
#
# Functions
#
err() {
  echo "$1...exiting"
  exit 1 # any non-0 exit code signals an error";
}

# function to check return code of programs.";
# exits with standard message if code is non-zero;";
# otherwise displays completiong message and date.";
#   arg1 is the return code (usually $?)";
#   arg2 is text describing what ran";
ckRes() {
  if [ "$1" == "0" ]; then
    echo "..Done $2 `date`"
  else
    err "$2 returned non-0 exit code $1"
  fi
}

# General usage statement
usage() { 
  echo "Usage: Run this script (atacseq_summarize.sh) in the directory where you want the results ouptut to with the following args.
  -d path to the sample directory (use full path to the directory)
  -n name -- sample name that matches all samples (e.g. Sample_KL)
  -i input -- the input (control) sample (optional).
  -g genome build -- hg19, hg38 (default), mm9, mm10
  -a annotation -- gencode (default), igenomes, refseq
  -e email -- user's email (do not use hss.edu)
  -h help -- prints this usage message" 
  exit 1
}

#
# Variables
#
INDIRPATH=""    # Default dir is the current working directory
NAME=""		   # The name common to all samples in the experiment
INPUT=""	   # Optional parameter for input control sample
GENOME="hg38"      # The genome build that that the samples were aligned to
ANNO="gencode"     # The annotation that was used to count features
EMAIL="hssgenomics@gmail.com"
ANNOLOC=/athena/hssgenomics/scratch/genomes_2/${GENOME}/Annotation/${ANNO}.latest.gtf #location of our genomes.
#
# Parse command-line arguments
#
while getopts :d:n:i:g:a:e:h: opt; do
  case $opt in
    d)
      INDIRPATH=$OPTARG
      ;;
    n)
      NAME=$OPTARG
      ;;
    i)
      INPUT=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    a)
      ANNO=$OPTARG
      ;;
    e)
      EMAIL=$OPTARG
      ;;
    h)
      usage
      ;;
    *) 
      usage
      ;;
  esac
done

#
# Check validity of inputs
#
if [ ! -d $INDIRPATH ] || [ "$INDIRPATH" == "" ]; then
  echo "Invalid input directory. \n"
  usage
elif [ `find . -maxdepth 1 -name "$NAME*" 2>/dev/null | wc -l` == 0 ] || [ "$NAME" == "" ]; then
  echo "Invalid name argument. \n"
  usage
elif [[ ! $GENOME =~ hg19|hg38|mm9|mm10 ]]; then
  echo "Invalid genome build selected. \n"
  usage
elif [ "$EMAIL" == "" ]; then
  echo "Invalid email address, please enter a valid email. \n"
  usage
fi

# set variables and load packages
TOPDIR=$INDIRPATH/$NAME
GENOMEPATH=/athena/hssgenomics/scratch/genomes_2/$GENOME
CHRSIZE=$GENOMEPATH/Genome/chrom.sizes
GENOMEMAP=$GENOMEPATH/Genome/chrom.data
#SAMPCORR=/athena/hssgenomics/scratch/programs/R/3.4.0/detools/ATACSampleCorr.r
GENOMESIZE=`awk '{ map = $2 * $3 ; tot += map ; print tot }' $GENOMEMAP | tail -n 1`

#r@3.5.0 no longer use R, instead uses featureCounts directly
spack load py-macs2\@2.2.4/iu6dcyy
# spack load bedtools2\@2.27.0 use locally install bedtools instead
spack load python\@3.8.0/coxzyuo
# spack load python\@3.6.0/zgvcgxo
spack load py-numpy@1.18.1/uk742f5
# spack load py-numpy\@1.15.1/5osntos


# Step 1: aggregate samples for summarization
#
TOCOPY=`find $INDIRPATH -maxdepth 1 -name "$NAME*"`
SHCOPY=`find $INDIRPATH -maxdepth 1 -name "*$NAME*.sh"`
echo "creating $TOPDIR and moving $TOCOPY files there..."
mkdir $TOPDIR; cd $TOPDIR; mv $TOCOPY $TOPDIR; mv $SHCOPY $TOPDIR

#
# Step 2: Call peaks with macs2
# So, for Pol2 I don't think we should be calling peaks. Just use genes as annotation

#mkdir PEAKS; PEAKSDIR=$TOPDIR/PEAKS/; cd $PEAKSDIR

# forking peak calling
#macs2CallPeaksWithInput(){
#  time macs2 callpeak -f BAM --nomodel --shift -100 --extsize 200 -B --SPMR -g $GENOMESIZE -q 0.01 -t $1 -c $CONTROL -n $2
#}
#macs2CallPeaksNoInput(){
#  time macs2 callpeak -f BAM --nomodel --shift -100 --extsize 200 -B --SPMR -g $GENOMESIZE -q 0.01 -t $1 -n $2 
#}

#JOBS=()
#if [ ! "$INPUT" == "" ]; then
#  TREAT=(`find $TOPDIR -name "${NAME}*.bam" | grep -v $TOPDIR/$INPUT`)
#  CONTROL=`find $TOPDIR -name "${NAME}*.bam" | grep $TOPDIR/$INPUT`
#  for (( i=0; i<${#TREAT[*]}; i++ )); do
#    TREATNAME=`basename ${TREAT[$i]%.aligned*}`
#    echo "Begin Peak Calling...`date`"
#    macs2CallPeaksWithInput ${TREAT[$i]} $TREATNAME 2> ${TREATNAME}.log.txt &
#    JOBS[${i}]=$!
#  done
#elif [ "$INPUT" == "" ]; then
#  TREAT=(`find $TOPDIR -name "${NAME}*.bam"`)
#  for (( i=0; i<${#TREAT[*]}; i++ )); do
#    TREATNAME=`basename ${TREAT[$i]%.aligned*}`
#    echo "Begin Peak Calling ... `date`"
#    macs2CallPeaksNoInput ${TREAT[$i]} $TREATNAME 2> macs2.log.txt &
#    JOBS[${i}]=$!
#  done
#fi
#for pid in ${JOBS[*]}; do
#  wait $pid
#done
#echo "Done Calling Peaks...`date`"
#
# Step 3 convert bdg to bw
#
spack unload py-macs2\@2.2.4/iu6dcyy
spack unload python\@3.8.0/coxzyuo
spack unload py-numpy@1.18.1/uk742f5

source /athena/hssgenomics/scratch/programs/deepTools2/bin/activate
mkdir $TOPDIR/TRACKS
cd $TOPDIR/TRACKS
BAMS=$(find $TOPDIR -name "*.bam")
for i in $BAMS; do
  bamCoverage -b $i -o ${i%.bam}.bw -of "bigwig" -p 12 --binSize 20 --smoothLength 60 --normalizeUsing BPM --ignoreForNormalization chrY chrX chrM;
done
cd $TOPDIR
echo "Done Converting bam to BigWig...`date`"

#
# Step 4: Correlation analysis. 
#    (1) use GTF gene annotation
#    (2) count reads in genes
#
#cd $PEAKSDIR
#cat $(ls *.narrowPeak) | cut -f 1,2,3,4 | sort -k 1,1 -k 2,2n | \
#bedtools merge -d 10 -c 4 -o collapse -i - | cut -d ',' -f 1 | \
#bedToGenePred stdin stdout | genePredToGtf file stdin stdout | \
#tr '\000' '.' > ${NAME}.union.gtf
#ckRes $? "Generating peaks GTF."
# find bam files in the topdir
BAMFILES=`find $TOPDIR -name ${NAME}*.bam | tr '\n' ' '`

time featureCounts -p -T 12 -f -t gene -g gene_id -a ${ANNOLOC} -o ${NAME}.counts.txt $BAMFILES
ckRes $? "Counting reads per feature."
cd $TOPDIR

#
# Step 2: run multiqc
#
#spack unload py-macs2\@2.2.4/iu6dcyy
#spack unload python\@3.8.0/coxzyuo
#spack unload py-numpy@1.18.1/uk742f5

source /athena/hssgenomics/scratch/programs/MultiQC/bin/activate
cd $TOPDIR
multiqc $TOPDIR # waiting for spack version
ckRes $? "Running MultiQC."
deactivate

echo "Summarization completed without errors. Exiting..."
exit 0
