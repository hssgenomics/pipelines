#!/bin/bash
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:RNAseq rnaseq_scheduler.sh | tar xf - > rnaseq_scheduler.sh 
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:RNAseq slurmAligner.template | tar xf - > slurmAligner.template
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:RNAseq slurmSummary.template | tar xf - > slurmSummary.template
chmod a+x rnaseq_scheduler.sh

exit 0 
