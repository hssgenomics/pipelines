#!/bin/bash
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:ATACseq ATAC_alignment.pl | tar xf - > ATAC_alignment.pl
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:ATACseq ATAC_callpeaks.sh | tar xf - > ATAC_callpeaks.sh
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:ATACseq ATAC_summarize.sh | tar xf - > ATAC_summarize.sh
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:ATACseq ATAC_pipeline.sh | tar xf - > ATAC_pipeline.sh
if [ "$1" == "with-tests" ]; then
  git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git \
      HEAD:ATACseq test/ | tar xf - 
fi
chmod a+x ATAC_alignment.pl ATAC_callpeaks.sh ATAC_summarize.sh ATAC_pipeline.sh

exit 0 
