# RNA-seq Pipeline

An up to date version of these pipeline scripts exist on the Cornell cluster at 
`/athena/hssgenomics/scratch/pipelines/rnaseq/`.
The Current version of these pipelines is `v2.1`.

There are 2 different pipelines for RNA-seq – single end (SE) and paired end (PE). 
Both of these pipelines align and count features with [STAR aligner](https://github.com/alexdobin/STAR). 

## Acquiring and Preparing the Data

The HSS Genomics Center's RNA-seq pipeline can now be used to take data from the sequencer 
through alignment, analysis, and finally visualization via Cornell server scripts, local 
statistical analysis, and finally shiny server visualiation platform.

The process for getting data from the sequencer to the visualization platform works as follows:

1. If the Genomics Core performed the sequencing then the data will be located in 
`/athena/hssgenomics/ingest/hssxfer`.
Otherwise, if the sequencing was performed at the Epigenomics Core it can be downloaded from 
[Pubshare](https://abc.med.cornell.edu/pubshare/).
Move or download the fastq files to the appropriate researcher's Fastq directory.  

2. The sequencing directory should be structured as folows: 
``` 
   |-- unique_run_id 
       |-- unique_project_id 
           |-- **Sample_ExperimentID_SEX_GENOTYPE_CELLTYPE_TREATMENT_TIME_REPLICATE**
               |-- Sample_xxx_001_R1.fq
               |-- Sample_xxx_002_R1.fq
```
NOTE: Not all variables are required. Either include OOO for missing information or exclude as desired.
 
## Running the Alignment Pipeline

3. The alignment pipeline and summarization script are run on these data. 
First migrate to the head node `panda2` via  
`ssh panda2.pbtech`  
Note: You will be asked for username/password combination. 
It is strongly recommended that you set up an RSA key pair for all your server access points.  

WARNING: **NEVER do anything on the head node.** It is only to be used to be assigned a node.

4. Migrate to a node that can see the storage drive `athena`.  
  `qrsh -l mem_free=1G -l athena=true`

5. Move to the project's subdirectory within the researcher's `Analysis` directory.
If it doesn't exist yet (it will not if this is a new project) then create it.  
`cd  /athena/hssgenomics/scratch/labs/thePI/theResearcher/Analysis/unique_run_id/unique_project_id/`

6. To run the pipeline specify the _full_ path to the `rnaseq_pipeline_STAR_PE(SE).sh` 
script with the following arguments supplied:  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-i Input directory.**  The project's subdirectory within the researcher's `Fastq` directory containing individual directories for each sample with gzipped fastq files e.g.  

```.  
├── [4.0K]  Sample_KO_Dex  
│   └── [7.0G]  Sample_KO_Dex.fastq.gz  
├── [4.0K]  Sample_KO_IFNI  
│   └── [7.7G]  Sample_KO_IFNI.fastq.gz  
```
  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-m Memory** Amount of memory to use for alignment. Default is 48G  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-t Threads** Number of threads to use for the alignment. Default is 6 threads.
  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-g Genome build** Genome to which reads are aligned. Currently available builds hg19, hg38, mm9, mm10. Any genome of interest can be added by request.
  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-e Email** The destination for emails regarding completion or failure of pipelines. Make sure your ISP does not block automated emails – HSS does! 

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-b keepBam** Should BAM alignment files be kept or should they be converted to BigWig. This option is included to reduce storage cost for alignment data. Options are TRUE (BAM is kept and no BigWig is generated) or FALSE (Default: BAM is converted to BigWig).
    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-a Gene annotation** The genomic coordinates and meta data for genomic features corresponding to the genome build. Currently available annotations include:  
  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hg19 – gencode, igenome, refseq  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hg38 – gencode, refseq  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mm9 – gencode, igenome  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mm10 – gencode, igenome
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rn6 – ensembl
  
Example of final command: 
```
/athena/hssgenomics/scratch/pipelines/rnaseq/rnaseq_pipeline_STAR_PE_v2.sh -t 6 -m 8G -g mm10 -a gencode -i /athena/hssgenomics/scratch/labs/thePI/theResearcher/Fastq/Some_Run_ID/Some_Project_ID -b FALSE -e some_email@yahoo.com
```

7. You will receive an email upon completion of each sample within the `input directory`. 
The output from the pipeline is a directory for each sample in the `input directory` with 
`STAR` appended to it. In addition, the submitted bash script and log files are also output.  

## Running the Summarization Script

8. Once all samples have completed, the summarization script can be run. 
Since this script requires minimal resources it can be run as a standard bash script without
submitting it to a node using the following parameters:  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-s Strandedness of library** Indicates the strandedness of the library prep kit. One of `none`, `forward`, or `reverse`. 

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-n Name** The name that is common to all samples in the experiment. (note: an underscore is supplied so if `sample_projectID_` matches all your samples omit the last underscore)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-g Genome build** The genome build chosen for the alignment (see above for available options).

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-a Gene annotation** The genomic feature annotation for the chosen genome build (see above for available options).

  Example of final command:
```
/athena/hssgenomics/scratch/pipelines/rnaseq/rnaseq_summarize_STAR.sh -s none -n sample_KO -g mm10 -a gencode
```

## Performing the Analysis

9. Once the summarization has run, move to your local machine and copy the `countMatrix.tsv`
and `multiqc_report.html` to your local machine. Additionally, copy the `anno.rds` from the 
approriate genome annotation directory. Finally, clone the Shiny project from the GitLab repo
to the same location and transfer `DETools.r`, `c2.cp.v6.2.symbols.gmt`, and `DEAnalysis.r`
to the top directory where your analysis will be run.  

10. Using `DEAnalysis.r`, run the analysis in order to build the edgeR.rds object for use
on the data visualization platform.  

## Setting Up the Visualization Platform

12. Access the data visualization server and and run the `addProject.sh` in the home directory with the following parameters:  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-n User name** This is the user's parent directory it should be last name and first initial e.g. OliverD. Linux is case sensitive so if the user exists and you supply improper capitalization a new directory structure will be produced.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**-p Project ID** This project ID should be generated by running the `md5sum` command on the `.rds` output from `DEAnalysis.r` but is not strictly enforced.

The resulting directory structure will be produced as shown below.

```
├── data_server
│   └── UserN                                   <\- Researcher (lastname followed by first initial)
│       └── cac9df92fb32486623f0fa9a10cbd854    <\- md5sum used as the unique project ID
│           ├── Data                            <\- Shared data directory
│           └── Shiny                           <\- Shiny App directory
```

13. Transfer the xlsx output from the `getDE()` function as well as the multiqc_report.html to newly created `Data` directory.  

14. Transfer the rds output from the `getDE()` function to the newly created `Shiny` directory.  

Your final directory stucture should look like the one shown below.

```.
├── Data
│   ├── edgeR_DEGs_2018-09-06.xlsx
│   └── multiqc_report.html
└── Shiny
    ├── app.R
    ├── edgeR_DEGs_2018-09-06.rds
    └── www
        └── logo.jpg
```

16. Test that it works by going to 10.225.108.167:3838/lastFirstInitial/md5sum/Shiny/

